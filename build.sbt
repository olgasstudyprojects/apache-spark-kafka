import Keys._ 

name := "CourseraCapstoneSpark"

version := "1.0"

scalaVersion := "2.10.6"

resolvers ++= Seq(
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Maven Repository" at "http://mvnrepository.com/artifact/",
  "Sonatype OSS Repo" at "https://oss.sonatype.org/content/repositories/releases",
  "Sonatype OSS Snapshots Repo" at "http://oss.sonatype.org/content/repositories/snapshots",
  "Sonatype OSS Tools Repo" at "https://oss.sonatype.org/content/groups/scala-tools",
  "Concurrent Maven Repo" at "http://conjars.org/repo",
  "Clojars Repository" at "http://clojars.org/repo",
  "Cloudera Repo" at "http://repository.cloudera.com/artifactory/cloudera-repos/",
  "Twitter Maven Repo" at "http://maven.twttr.com"
)

libraryDependencies ++= Seq(
  "org.apache.commons" % "commons-lang3" % "3.4",
  "com.datastax.spark" %% "spark-cassandra-connector" % "1.5.0-RC1",
  "com.datastax.cassandra" % "cassandra-driver-core" % "3.0.0-rc1",
  "org.apache.spark" %% "spark-core" % "1.6.0",
  "org.apache.spark" %% "spark-streaming" % "1.6.0",
  "org.apache.spark" %% "spark-streaming-kafka" % "1.6.0"
)


// There is a conflict between Guava versions on Cassandra Drive and Hadoop
// Shading Guava Package
assemblyShadeRules in assembly := Seq(
  ShadeRule.rename("com.google.**" -> "shadeio.@1").inAll
)

mergeStrategy in assembly := {
  case m if m.toLowerCase.endsWith("manifest.mf")          => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf.*\\.sf$")      => MergeStrategy.discard
  case "log4j.properties"                                  => MergeStrategy.discard
  case m if m.toLowerCase.startsWith("meta-inf/services/") => MergeStrategy.filterDistinctLines
  case "reference.conf"                                    => MergeStrategy.concat
  case _                                                   => MergeStrategy.first
}