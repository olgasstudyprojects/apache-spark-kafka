import common.{CountsTupleReducer, KafkaStreamer, InputLine}
import org.apache.spark.streaming.dstream.DStream

object OnTimeWeekdays extends KafkaStreamer with CountsTupleReducer {

  def emitWeekdayData(line: String) = {
    val parsedLine = new InputLine(line)
    val onTimeCounter = if(parsedLine.arrOnTime) 1 else 0
    if(!parsedLine.arrDelayMinutes.isNaN) {
      List((parsedLine.dayOfWeek, (onTimeCounter, 1)))
    } else {
      List()
    }
  }

  def updateCounters(newValues: Seq[(Int, Int)], currentValue: Option[Seq[(Int, Int)]]) = {
    val allDataForKey = (currentValue.getOrElse(Seq.empty) ++ newValues).reduce(reduceIntCounts)
    Some(Seq(allDataForKey))
  }

  override def process(lines: DStream[String]): Unit = {
   lines.flatMap(emitWeekdayData)
     .reduceByKey(reduceIntCounts).updateStateByKey(updateCounters)
     .map {case (day, Seq((onTime, total)))  => (day, (onTime.toFloat / total.toFloat) * 100)}
     .repartition(1)
     .transform(dataset => dataset.sortBy({case (day, percentage) => percentage}, false))
     .foreachRDD(rdd => rdd.saveAsTextFile(args(2)))
  }
}