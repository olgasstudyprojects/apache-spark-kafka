import com.datastax.spark.connector._
import com.datastax.spark.connector.SomeColumns
import common.{TopRatingState, KafkaStreamer, BayesianRating, InputLine}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{StateSpec, State}
import org.apache.spark.streaming.dstream.DStream
import com.datastax.spark.connector.streaming._

object AirportCarrierTopDeparture extends KafkaStreamer with TopRatingState {
  val db_keyspace = "airlines_spark"
  val db_table = "airport_carriers_top_departure"

  def emitDeparturePerformance(line: String): List[((String, String), (Int, Int))] = {
    val parsedLine = new InputLine(line)
    if (parsedLine.depDelayMinutes.isNaN) return List()
    val onTimeEventCount = if (parsedLine.depOnTime) 1 else 0
    List(((parsedLine.origin, parsedLine.uniqueCarrier), (onTimeEventCount, 1)))
  }

  override def configure = {
    val conf = new SparkConf().setAppName("Top 10 carriers for every origin")
    conf.set("spark.cassandra.connection.host", args(3))
  }

  //basic processing flow. See common.TopRatingState for explicit methods to update the global persisted state
  override def process(lines: DStream[String]): Unit = {
    val initialDeparturePerformance = 0.84F
    val threshold = 10000
    lines.flatMap(emitDeparturePerformance)
      .reduceByKey(reduceIntCounts)
      .mapWithState(StateSpec.function(updatedCounters _))
      .map {
        case ((origin, carrier), (onTime, total)) =>
          val rateAdjusted = new BayesianRating(initialDeparturePerformance, threshold)
          (origin, (carrier, rateAdjusted.weightedRating(onTime, total)))
      }
      .mapWithState(StateSpec.function(updatedRatings _))
      .saveToCassandra(db_keyspace,
        db_table,
        SomeColumns("origin", "top10_rating"))
  }
}
