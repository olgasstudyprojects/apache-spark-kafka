import common.KafkaStreamer
import org.apache.spark.streaming.dstream.DStream
import TopPopularAirports.{emitAirportData, updateCounters}

object AirportPopularityDistribution extends KafkaStreamer {

  override def process(lines: DStream[String]): Unit = {
    val sortedPopularAirports = lines.flatMap(emitAirportData)
      .reduceByKey(_ + _)
      .updateStateByKey(updateCounters)
      .repartition(1)
      .transform(rdd => rdd.sortBy({case (airport, flightsCount) => flightsCount}, false))
      .map {case (airport, flightsCount) => airport + '\t' + flightsCount}
      .foreachRDD(rdd => {rdd.saveAsTextFile(args(2))})
  }
}
