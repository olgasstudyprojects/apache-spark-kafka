import com.datastax.spark.connector.SomeColumns
import common.{CountsTupleReducer, KafkaStreamer, InputLine}
import com.datastax.spark.connector.streaming._
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{StateSpec, State}
import org.apache.spark.streaming.dstream.DStream

object MeanArrivalDelay extends KafkaStreamer with CountsTupleReducer {

  def emitDelays(line: String) = {
    val parsedLine = new InputLine(line)
    if(!parsedLine.arrDelayMinutes.isNaN) {
      List(((parsedLine.origin, parsedLine.destination), (parsedLine.arrDelayMinutes, 1)))
    } else {
      List()
    }
  }

  val updatedDelay = (key: (String, String), newDelay: Option[(Float, Int)],
                      currentDelays: State[(Float, Int)]) => {
    val sumDelays = reduceDelays(
      currentDelays.getOption().getOrElse((0F, 0)),
      newDelay.getOrElse(0F, 0)
    )
    currentDelays.update(sumDelays)
    (key._1, key._2, sumDelays._1 / sumDelays._2)
  }

  override def configure = {
    val conf = new SparkConf().setAppName("Mean arrival delay")
    conf.set("spark.cassandra.connection.host", args(3))
  }

  override def process(lines: DStream[String]) = {
    lines.flatMap(emitDelays)
      .reduceByKey(reduceDelays)
      .mapWithState(StateSpec.function(updatedDelay))
      .saveToCassandra("airlines_spark",
        "mean_arrival_delays",
        SomeColumns("origin", "destination", "mean_delay"))
  }
}
