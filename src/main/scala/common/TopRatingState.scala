package common

import com.datastax.spark.connector.TupleValue
import org.apache.spark.streaming.State

import scala.collection.mutable

trait TopRatingState extends CountsTupleReducer {

  type MetricAndRating = (String, Float)

  def updatedCounters(key: (String, String), newCounter: Option[(Int, Int)],
                      currentCounter: State[(Int, Int)]) = {
    val finalCounter = reduceIntCounts(newCounter.getOrElse(0, 0),
      currentCounter.getOption().getOrElse(0, 0))
    currentCounter.update(finalCounter)
    (key, finalCounter)
  }

  def updatedRatings(key: String, newDelay: Option[MetricAndRating],
                                          currentDelays: State[mutable.HashMap[String, MetricAndRating]]) = {
    val delays = currentDelays.getOption().getOrElse(new mutable.HashMap)
    if(newDelay.isDefined) {
      delays += (newDelay.get._1 -> newDelay.get)
    }
    val result = delays.values.toList.sortBy({case(dest, rating) => -rating}).take(10)
    currentDelays.update(delays)
    (key, result)
  }
}
