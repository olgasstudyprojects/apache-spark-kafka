package common

// For the explanation of the formula, see http://fulmicoton.com/posts/bayesian_rating/

class BayesianRating(initialProbability: Float, eventsThreshold: Int)  {
  def weightedRating(ratedEventsNumber: Int, totalEventsNumber: Int) = {
    val totalWithThreshold = eventsThreshold + totalEventsNumber
    val initialRating = initialProbability * eventsThreshold
    (initialRating + ratedEventsNumber.toFloat) / totalWithThreshold.toFloat
  }
}
