package common

import java.text.SimpleDateFormat
import java.util.Date

class InputLine(source: String) extends Serializable {
  val parts = source.replace(System.lineSeparator(), "").split("\\|", -1)
  val airlineId = parts(0)
  val uniqueCarrier = parts(1)
  val origin = parts(2)
  val destination = parts(3)
  private val sdf = new SimpleDateFormat("yyyy-MM-dd")

  var flightDate: Date = try {
    sdf.parse(parts(4))
  } catch {
    case e: Exception => {
      e.printStackTrace()
    }
    null
  }

  val year = parts(5)
  val dayOfWeek = parts(6)
  val depTime = parts(7)
  val depDelayMinutes = if(parts(8).equals("")) Float.NaN else parts(8).toFloat
  val depOnTime = parts(9).equals("0.00")
  val arrTime = parts(10)
  val arrDelayMinutes = if(parts(11).equals("")) Float.NaN else parts(11).toFloat
  val arrOnTime = parts(12).equals("0.00")
  val flightNo = parts(13)
}
