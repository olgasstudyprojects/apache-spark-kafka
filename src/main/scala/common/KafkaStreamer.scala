package common

import kafka.serializer.StringDecoder
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

trait KafkaStreamer {
  var conf:SparkConf = _
  var args: Array[String] = _
  var streaming: StreamingContext = _

  def main(args: Array[String]): Unit = {
    this.args = args
    this.conf = configure
    this.streaming = new StreamingContext(conf, Seconds(1))

    val kafkaParams = Map[String, String]("metadata.broker.list" -> args(0))
    val topicsSet = args(1).split(",").toSet

    val lines = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      streaming, kafkaParams, topicsSet).map(_._2)

    process(lines)

    streaming.checkpoint(args(2))
    streaming.start
    streaming.awaitTermination
  }

  //can be overridden
  def configure = {
    new SparkConf().setAppName("Kafka streaming application")
  }

  def process(lines: DStream[String])
}
