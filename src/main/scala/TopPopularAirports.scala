import common.{KafkaStreamer, InputLine}
import org.apache.spark.streaming.dstream.DStream

object TopPopularAirports extends KafkaStreamer {
  def emitAirportData(line: String) = {
    val parsedLine = new InputLine(line)
    List((parsedLine.origin, 1), (parsedLine.destination, 1))
  }

  def updateCounters(newValues: Seq[Int], currentValue: Option[Int]) = {
    val allDataForKey = newValues.sum + currentValue.getOrElse(0)
    Some(allDataForKey)
  }

  override def process(lines: DStream[String]) = {
    lines.flatMap(emitAirportData)
      .reduceByKey(_ + _)
      .updateStateByKey(updateCounters)
      .repartition(1)
      .transform(dataset => dataset.sortBy({case (airport, flightsCount) => flightsCount}, false))
      .foreachRDD(rdd => streaming.sparkContext.parallelize(rdd.take(10), 1).saveAsTextFile(args(2)))
  }
}