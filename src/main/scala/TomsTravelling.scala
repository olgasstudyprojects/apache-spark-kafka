import java.util.Date

import com.datastax.spark.connector.SomeColumns
import common.{InputLine, KafkaStreamer}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{StateSpec, State}
import org.apache.spark.streaming.dstream.DStream
import com.datastax.spark.connector.streaming._
import com.datastax.spark.connector._


object TomsTravelling extends KafkaStreamer {
  type FlightKey = (String, String, Date, Boolean)

  private val targetYear = "2008"
  private def over12 = (timeStr: String) => timeStr > "1200"

  private def bestFlight(flight1: InputLine, flight2: InputLine) = {
    if(flight2.arrDelayMinutes <= flight1.arrDelayMinutes)
      flight2
    else
      flight1
  }

  val bestFlightEver = (key: FlightKey, newFlight: Option[InputLine], currentFlight: State[InputLine]) => {

    val currentFlightOption = currentFlight.getOption()

    val bestFlightOption = if(newFlight.isDefined && currentFlightOption.isEmpty) {
      newFlight
    } else if (newFlight.isDefined && currentFlightOption.isDefined) {
      Some(bestFlight(newFlight.get, currentFlightOption.get))
    } else {
      currentFlightOption
    }
    //if it has reached this call, it's never empty
    val best = bestFlightOption.get
    (best.origin, best.destination, best.flightDate, over12(best.depTime), best.depTime,
      best.uniqueCarrier, best.flightNo, best.arrDelayMinutes)

  }

  override def configure = {
    val conf = new SparkConf().setAppName("Tom's travelling options")
    conf.set("spark.cassandra.connection.host", args(3))
  }

  def emitFlightOptions(line: String): List[(FlightKey, InputLine)] = {
    val parsedLine = new InputLine(line)
    // no cancelled flights!
    if(parsedLine.arrDelayMinutes.isNaN || parsedLine.year != targetYear) {
      Nil
    } else {
      List(
        //key: origin, destination and date of the first flight, with a flag for time > 1200
        ((parsedLine.origin, parsedLine.destination, parsedLine.flightDate, over12(parsedLine.depTime)),
          //value: all other payload data
          parsedLine)
      )
    }
  }

  override def process(lines: DStream[String]) = {
    lines.flatMap(emitFlightOptions)
      .reduceByKey(bestFlight)
      .mapWithState(StateSpec.function(bestFlightEver))
      .saveToCassandra("airlines_spark",
        "toms_travelling",
        SomeColumns("origin", "destination", "date", "is_afternoon", "dep_time",
          "carrier", "flight_no", "arrival_delay"))
  }
}
